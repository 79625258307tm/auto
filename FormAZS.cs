﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auto
{
    public partial class FormAZS : Form
    {
        string currentModelPath = Directory.GetCurrentDirectory() + "\\CurrentModel.txt";

        public FormAZS()
        {
            InitializeComponent();
        }

        private void FormAZS_Load(object sender, EventArgs e)
        {
            trackBar2.Maximum = Convert.ToInt32(File.ReadLines(currentModelPath).Skip(9).First()) - FormMain.Benz;
            trackBar2.SmallChange = 1;
            trackBar2.LargeChange = 5;
            label3.Text = trackBar2.Maximum.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormMain.Benz += trackBar2.Value;
            Close();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar2.Value.ToString();
        }
    }
}
