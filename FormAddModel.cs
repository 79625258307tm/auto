﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auto
{
    public partial class FormAddModel : System.Windows.Forms.Form
    {
        public FormAddModel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text  != "" && textBox4.Text  !=  "" && textBox5.Text != "" && textBox7.Text != "" &&
                textBox8.Text != "" && textBox9.Text != "" && textBox10.Text != "" && textBox12.Text != "") {
                string path = Directory.GetCurrentDirectory() + "\\Models\\" + textBox1.Text + ".txt";
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Model");
                    sw.WriteLine(textBox1.Text);
                    sw.WriteLine(textBox2.Text);
                    sw.WriteLine(textBox3.Text);
                    sw.WriteLine(textBox4.Text);
                    sw.WriteLine(textBox5.Text);
                    sw.WriteLine(comboBox1.Text);
                    sw.WriteLine(textBox7.Text);
                    sw.WriteLine(textBox8.Text);
                    sw.WriteLine(textBox9.Text);
                    sw.WriteLine(textBox10.Text);
                    sw.WriteLine(textBox12.Text);
                    if (checkBox1.Checked) sw.WriteLine("Есть");
                    else sw.WriteLine("Нет");
                }
                Close();
            }
        }



        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormAddModel_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && (e.KeyChar != (char) Keys.Back)) {
                MessageBox.Show("Введите число!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
            }
        }
    }
}
