﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auto
{
    public partial class FormAutoService : Form
    {
        string currentModelPath = Directory.GetCurrentDirectory() + "\\CurrentModel.txt";
        public FormAutoService()
        {
            InitializeComponent();
        }

        private void FormAutoService_Load(object sender, EventArgs e)
        {
            trackBar2.Maximum = Convert.ToInt32(Convert.ToInt32(File.ReadLines(currentModelPath).Skip(7).First())*2.5 - FormMain.Oil);
            trackBar2.TickFrequency = trackBar2.Maximum / 20;
            trackBar2.SmallChange = 10;
            trackBar2.LargeChange = 50;
            label3.Text = trackBar2.Maximum.ToString();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar2.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormMain.Oil += trackBar2.Value;
            Close();
        }
    }
}
