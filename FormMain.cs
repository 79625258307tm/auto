﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auto
{
    public partial class FormMain : System.Windows.Forms.Form
    {
        public static int Benz;
        public static int Oil;
        public static int Probeg;
        public static int dProbeg;
        int delta;
        string currentModelPath = Directory.GetCurrentDirectory() + "\\CurrentModel.txt";
        string currentModelProperties = Directory.GetCurrentDirectory() + "\\CurrentModelProperties.txt";
        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Установка и демонтаж БК
            if (button1.Text == "Установка БК")
            {
                ButtonsChange(true);
                using (StreamWriter sw = File.CreateText(currentModelPath))
                {
                    sw.WriteLine(label1.Text.Remove(0, "Модель: ".Length));
                    sw.WriteLine(label12.Text.Remove(0, "Марка: ".Length));
                    sw.WriteLine(label2.Text.Remove(0, "Габариты (ДxШxВ, мм): ".Length));
                    sw.WriteLine(label3.Text.Remove(0, "Масса (кг): ".Length));
                    sw.WriteLine(label4.Text.Remove(0, "Год выпуска: ".Length));
                    sw.WriteLine(label6.Text.Remove(0, "Класс БК: ".Length));
                    sw.WriteLine(label5.Text.Remove(0, "Уд. мощность двигателя (кг/л.с.): ".Length));
                    sw.WriteLine(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length));
                    sw.WriteLine(label7.Text.Remove(0, "Средний расход (л/100км): ".Length));
                    sw.WriteLine(label9.Text.Remove(0, "Объём бензобака (л): ".Length));
                    sw.WriteLine(label11.Text.Remove(0, "Емкость аккумулятора (А*ч): ".Length));
                    sw.WriteLine(label10.Text.Remove(0, "Кондиционер: ".Length));
                }
                using (StreamWriter sw = File.CreateText(currentModelProperties))
                {
                    sw.WriteLine(label9.Text.Remove(0, "Объём бензобака (л): ".Length));
                    sw.WriteLine(0);
                    sw.WriteLine(Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length))*2.5));
                    delta = Convert.ToInt32(Convert.ToDouble(label7.Text.Remove(0, "Средний расход (л/100км): ".Length)) * 20 * 15);
                    // sw.WriteLine(Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length))*2.5) - delta);
                }
                button7.Enabled = true;
            }
            else 
            {
                if (label28.Text == "Состояние:")
                {
                    if (MessageBox.Show("Все данные об автомобиле удалятся", "Внимание", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        ButtonsChange(false);
                        File.Delete(currentModelProperties);
                        File.Delete(currentModelPath);
                        label1.Text = "Модель:";
                        label12.Text = "Марка:";
                        label2.Text = "Габариты (ДxШxВ, мм):";
                        label3.Text = "Масса (кг):";
                        label4.Text = "Год выпуска:";
                        label6.Text = "Класс БК:";
                        label5.Text = "Уд. мощность двигателя (кг/л.с.):";
                        label8.Text = "Объём двигателя (куб. см):";
                        label7.Text = "Средний расход (л/100км):";
                        label9.Text = "Объём бензобака (л):";
                        label11.Text = "Емкость аккумулятора (А*ч):";
                        label10.Text = "Кондиционер:";
                        button7.Enabled = false;
                        Probeg = 0;
                    }
                }
                else { MessageBox.Show("Зажигание включено", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning); }  
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Добавить новую модель
            FormAddModel form = new FormAddModel();
            form.Show();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            //Первоначальная загрузка главной формы
            if (File.Exists(currentModelPath))
            using (StreamReader sr = File.OpenText(currentModelPath))
            {
                loadModel(sr);
            }
            updateFirst();
            if (label1.Text != "Модель:")
            {
                ButtonsChange(true);
            }
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            
            if (button1.Enabled) { button7.Enabled = true; }
        }

        public void updateFirst()
        {
            if (File.Exists(currentModelProperties))
                using (StreamReader sr = File.OpenText(currentModelProperties))
                {
                    delta = Convert.ToInt32(Convert.ToDouble(label7.Text.Remove(0, "Средний расход (л/100км): ".Length)) * 20 * 15);
                    Benz = Convert.ToInt32(sr.ReadLine());
                    Probeg = Convert.ToInt32(sr.ReadLine());
                    dProbeg = 0;
                    Oil = Convert.ToInt32(sr.ReadLine());
                }
        }
        public void update() {
            if (File.Exists(currentModelProperties))
                using (StreamReader sr = File.OpenText(currentModelProperties))
                {
                    progressBar2.Value = Benz;
                    progressBar2.Maximum = Convert.ToInt32(label9.Text.Remove(0, "Объём бензобака (л):".Length + 1));
                    label27.Text = "Пробег: " + Probeg.ToString();
                    label26.Text = "Текущий пробег: " + dProbeg.ToString();
                    progressBar3.Minimum = Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length)) * 2.5 - delta);
                    progressBar3.Maximum = Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length)) * 2.5);
                    progressBar3.Value = Oil;
                    if ((progressBar3.Value - progressBar3.Minimum) * 10 < (progressBar3.Maximum - progressBar3.Minimum)) { label30.Visible = true; pictureBox8.Visible = true; button6.Enabled = false; pictureBox7.Visible = false; }
                    else { label30.Visible = false; pictureBox8.Visible = false; if (pictureBox3.Visible == true) { button6.Enabled = true; pictureBox7.Visible = true; } }
                    if (progressBar2.Value * 10 < progressBar2.Maximum) { label29.Visible = true; pictureBox4.Visible = true; }
                    else { label29.Visible = false; pictureBox4.Visible = false; }

                }
        }
        public void saveProperties()
        {
            if (File.Exists(currentModelProperties))
                using (StreamWriter sw = File.CreateText(currentModelProperties))
                {
                    sw.WriteLine(Benz);
                    sw.WriteLine(Probeg);
                    sw.WriteLine(Oil);
                }
        }
        private void ButtonsChange(bool downloaded) {
            //Процедура смены кнопок во вкладке "Настройки"
            if (downloaded == true)
            {
                button1.Text = "Демонтаж";
                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
                button10.Enabled = false;
            }
            else
            {
                button1.Text = "Установка БК";
                button1.Enabled = false;
                button10.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        private void FormMain_Activated(object sender, EventArgs e)
        {
            if (label28.Text != "Состояние:") update();
            saveProperties();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //Выбрать модель
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedModelPath = openFileDialog.FileName;
                using (StreamReader sr = File.OpenText(selectedModelPath))
                    if (sr.ReadLine() == "Model")
                    {
                        loadModel(sr);
                        button1.Enabled = true;
                    }
            }
        }

        private void loadModel(StreamReader sr) {
                    label1.Text = "Модель: " + sr.ReadLine();
                    label12.Text = "Марка: " + sr.ReadLine();
                    label2.Text = "Габариты (ДxШxВ, мм): " + sr.ReadLine();
                    label3.Text = "Масса (кг): " + sr.ReadLine();
                    label4.Text = "Год выпуска: " + sr.ReadLine();
                    label6.Text = "Класс БК: " + sr.ReadLine();
                    label5.Text = "Уд. мощность двигателя (кг/л.с.): " + sr.ReadLine();
                    label8.Text = "Объём двигателя (куб. см): " + sr.ReadLine();
                    label7.Text = "Средний расход (л/100км): " + sr.ReadLine();
                    label9.Text = "Объём бензобака (л): " + sr.ReadLine();
                    label11.Text = "Емкость аккумулятора (А*ч): " + sr.ReadLine();
                    label10.Text = "Кондиционер: " + sr.ReadLine();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Удалить модель
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string deletePath = openFileDialog.FileName;
                using (StreamReader sr = File.OpenText(deletePath))
                    if (sr.ReadLine() == "Model")
                    {
                        sr.Close();
                        File.Delete(deletePath);
                    }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //Зажигание
            if (button8.Enabled == false)
            {
                label21.Text = "Tемпература в салоне: " + textBox1.Text;
                label22.Text = "Влажность: " + textBox2.Text;
                label26.Text = "Текущий пробег: " + dProbeg.ToString();
                label27.Text = "Пробег: " + Probeg.ToString();
                label28.Text = "Состояние: включено зажигание";
                pictureBox2.Visible = true;
                pictureBox5.Visible = true;
                pictureBox6.Visible = true;
                button8.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                trackBar1.Enabled = true;
                trackBar2.Enabled = true;
                if (label10.Text.Remove(0, "Кондиционер: ".Length) == "Есть") trackBar3.Enabled = true;
                using (StreamReader sr = File.OpenText(currentModelProperties))
                {
                    Benz = Convert.ToInt32(sr.ReadLine());
                    Oil = Convert.ToInt32(File.ReadLines(currentModelProperties).Skip(2).First());
                 //   Oil = Convert.ToInt32(sr.ReadLine());
                }
                progressBar2.Maximum = Convert.ToInt32(label9.Text.Remove(0, "Объём бензобака (л):".Length + 1));
                progressBar2.Value = Benz;
                progressBar3.Maximum = Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length)) * 2.5);
                progressBar3.Minimum = Convert.ToInt32(Convert.ToInt32(label8.Text.Remove(0, "Объём двигателя (куб. см): ".Length)) * 2.5 - delta);
                progressBar3.Value = Oil;
                if ((progressBar3.Value - progressBar3.Minimum) * 10 < (progressBar3.Maximum - progressBar3.Minimum)) { label30.Visible = true; pictureBox8.Visible = true; }
                if (progressBar2.Value * 10 < progressBar2.Maximum) { label29.Visible = true; pictureBox4.Visible = true; }
            }
            else 
            {
                trackBar1.Value = 0;
                trackBar2.Value = 0;
                trackBar3.Value = 0;
                pictureBox2.Visible = false;
                pictureBox5.Visible = false;
                pictureBox6.Visible = false;
                label28.Text = "Состояние:";
                label26.Text = "Текущий пробег: ";
                label27.Text = "Пробег: ";
                label21.Text = "Tемпература в салоне:";
                label22.Text = "Влажность:";
                button8.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
                trackBar1.Enabled = false;
                trackBar2.Enabled = false;
                trackBar3.Enabled = false;
                label30.Visible = false;
                pictureBox8.Visible = false;
                progressBar2.Value = 0;
                progressBar3.Value = progressBar3.Minimum;
                label29.Visible = false;
                pictureBox4.Visible = false;
            }
        }
        public void updateTemp()
        { 

        }

            private void button6_Click(object sender, EventArgs e)
        {
            //Эксплуатация
            FormDrive formDrive = new FormDrive();
            formDrive.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //Завести
            pictureBox7.Visible = true;
            pictureBox3.Visible = true;
            label28.Text = "Состояние: автомобиль заведён";
            button6.Enabled = true;
            if (label30.Visible == true) { button6.Enabled = false; pictureBox7.Visible = false; }
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Заправить
            FormAZS formAZS = new FormAZS();
            formAZS.Show();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
         //   saveProperties();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //Заглушить
            pictureBox3.Visible = false;
            pictureBox7.Visible = false;
            label28.Text = "Состояние: включено зажигание";
            button7.Enabled = true;
            button6.Enabled = false;
            button8.Enabled = true;
            button9.Enabled = false;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label22.Text = "Влажность: " + Convert.ToInt32(Convert.ToDouble(textBox2.Text) * (100 - trackBar1.Value * 9) / 100 * (100 + trackBar2.Value * 4.5) / 100 * (100 + trackBar3.Value * 4.7) / 100);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            if (trackBar2.Value > 0) trackBar3.Enabled = false;
            else if(label10.Text.Remove(0, "Кондиционер: ".Length) == "Есть") trackBar3.Enabled = true;
            label21.Text = "Tемпература в салоне: " + Convert.ToInt32(Convert.ToDouble(textBox1.Text)  + trackBar2.Value * Math.Abs(Convert.ToDouble(textBox1.Text) - 10) / 4);
            label22.Text = "Влажность: " + Convert.ToInt32(Convert.ToDouble(textBox2.Text) * (100 - trackBar1.Value * 9) / 100 * (100 + trackBar2.Value * 4.5) / 100 * (100 + trackBar3.Value * 4.7) / 100);
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            if (trackBar3.Value > 0) trackBar2.Enabled = false;
            else  trackBar2.Enabled = true;
            label21.Text = "Tемпература в салоне: " + Convert.ToInt32(Convert.ToInt32(textBox1.Text) - trackBar3.Value * 5);
            label22.Text = "Влажность: " + Convert.ToInt32(Convert.ToDouble(textBox2.Text) * (100 - trackBar1.Value * 9) / 100 * (100 + trackBar2.Value * 4.5) / 100 * (100 + trackBar3.Value * 4.7) / 100);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormAutoService formAS = new FormAutoService();
            formAS.Show();
        }
    }
}
