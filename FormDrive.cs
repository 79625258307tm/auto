﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auto
{
    public partial class FormDrive : Form
    {
        string currentModelPath = Directory.GetCurrentDirectory() + "\\CurrentModel.txt";
        private int Rash;
        private int Benz;
        public FormDrive()
        {
            InitializeComponent();
        }
       

        private void FormDrive_Load(object sender, EventArgs e)
        {
            Rash = Convert.ToInt32(File.ReadLines(currentModelPath).Skip(8).First());
            Benz = FormMain.Benz;
            trackBar2.Maximum = Convert.ToInt32(Convert.ToDouble(Benz) / Convert.ToDouble(Rash) * 100);
            trackBar2.TickFrequency = trackBar2.Maximum / 20;
            trackBar2.SmallChange = 5;
            trackBar2.LargeChange = 20;
            label3.Text = trackBar2.Maximum.ToString();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FormMain.Benz = Convert.ToInt32(Convert.ToDouble(Benz) - Convert.ToDouble(trackBar2.Value) / 100 * Convert.ToDouble(Rash));
            FormMain.Oil -= Convert.ToInt32(Convert.ToDouble(Rash)*0.02* trackBar2.Value);
            FormMain.dProbeg += trackBar2.Value;
            FormMain.Probeg += trackBar2.Value;
            Close();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar2.Value.ToString();
        }
    }
}
